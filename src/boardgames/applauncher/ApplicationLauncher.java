
package boardgames.applauncher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import boardgames.controller.GameControllerImpl;
import boardgames.view.game.MenuView;
import boardgames.view.game.MenuViewImpl;

/**
 * Class that starts the game.
 *
 */

public final class ApplicationLauncher {

    private ApplicationLauncher() {

    }

    /**
     * Main class.
     * 
     * @param args
     *            unused
     */
    public static void main(final String[] args) {

        final Logger logger = LogManager.getLogger(ApplicationLauncher.class.getName());
        logger.trace("Starting application");
        final GameControllerImpl controller = GameControllerImpl.getController();
        final MenuView v = new MenuViewImpl(controller);
        v.showMenu();
    }

}
