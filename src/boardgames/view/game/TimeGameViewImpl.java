package boardgames.view.game;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Optional;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import boardgames.controller.GameControllerImpl;
import boardgames.view.utility.GameFrameFactory;
import boardgames.view.utility.MainFrameFactory;
import boardgames.view.utility.PopUpFactory;

/**
 *Class responsible for creating and updating the components of time-based play mode.
 *
 */
public class TimeGameViewImpl implements TimeGameView {

    private static final JTextArea WHITETIMERAREA = MainFrameFactory.createJTextArea(Optional.empty(), false, false);
    private static final JTextArea BLACKTIMERAREA = MainFrameFactory.createJTextArea(Optional.empty(), false, false);
    private static final JTextArea PLAYTIMERAREA = MainFrameFactory.createJTextArea(Optional.empty(), false, false);
    private static final String TIMER = "timer";

    private final JPanel stopwatchWhitePanel;
    private final JPanel stopwatchBlackPanel;
    private final JPanel playTimePanel;

    /**
     * Constructor of the class that create the "time view" components like panels and labels.
     */
    public TimeGameViewImpl() {
        final GameFrameFactory mainFactory = new MainFrameFactory();

        this.stopwatchWhitePanel = mainFactory.cratePanel(Optional.empty(), new FlowLayout(), false);
        this.stopwatchBlackPanel = mainFactory.cratePanel(Optional.empty(), new FlowLayout(), false);
        this.playTimePanel = mainFactory.cratePanel(Optional.empty(), new FlowLayout(), true);

        this.stopwatchWhitePanel.add(mainFactory.createJLabel(Optional.of("White " + TIMER + ": ")));
        this.stopwatchBlackPanel.add(mainFactory.createJLabel(Optional.of("Black " + TIMER + ": ")));
        this.playTimePanel.add(mainFactory.createJLabel(Optional.of("Play time: ")));

        this.stopwatchWhitePanel.add(WHITETIMERAREA);
        this.stopwatchBlackPanel.add(BLACKTIMERAREA);
        this.playTimePanel.add(PLAYTIMERAREA);
    }

    @Override
    public final JPanel getWhitePanel() {
        return this.stopwatchWhitePanel;
    }

    @Override
    public final JPanel getBlackPanel() {
        return this.stopwatchBlackPanel;
    }

    @Override
    public final JPanel getPlayPanel() {
        return this.playTimePanel;
    }

    @Override
    public final void setTimerPanelVisible() {
        this.stopwatchWhitePanel.setVisible(true);
        this.stopwatchBlackPanel.setVisible(true);
        this.playTimePanel.setVisible(false);

    }

    /**
     * This method shows the flow of white player stopwatch.
     * @param timer string that contains time
     */
    public static void updateWhiteTimer(final String timer) {
        WHITETIMERAREA.setText(timer);

    }

    /**
     * This method shows the flow of black player stopwatch.
     * @param timer string that contains time
     */
    public static void updateBlackTimer(final String timer) {
        BLACKTIMERAREA.setText(timer);
    }

    /**
     * This method shows the flow of play timer.
     * @param timer string that contains time
     */
    public static void updatePlayTime(final String timer) {
        PLAYTIMERAREA.setText(timer);
    }

    /**
     * Method that shows the winning panel in a timed match that inform which player
     * has won.
     */
    public static void timedVictoryPanel() {
        String winner = null;
        final GameFrameFactory popFactory = new PopUpFactory();
        final JFrame frame = popFactory.createFrame(Optional.of("The timed match is over"));
        final JPanel panel = popFactory.cratePanel(Optional.of("Winning panel"), new GridBagLayout(), true);

        final GridBagConstraints gbc = popFactory.createGridBagConstraints();

        winner = GameControllerImpl.getController().getTimedWinner().equals(Optional.empty()) ? "Parity"
                : GameControllerImpl.getController().getTimedWinner().get().toString()
                        + " is the player with more pieces";

        panel.add(popFactory.createJLabel(Optional.of(winner)), gbc);
        gbc.gridy++;
        panel.add(popFactory.createButton(Optional.of(e -> System.exit(0)), Optional.of("EXIT")), gbc);
        frame.add(panel);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
