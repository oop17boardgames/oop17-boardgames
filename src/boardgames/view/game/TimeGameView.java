package boardgames.view.game;

import javax.swing.JPanel;

/**
 * Interface that provide methods to access the timed game's components.
 *
 */
public interface TimeGameView {

    /**
     * @return the panel that contains the white player stopwatch
     */
    JPanel getWhitePanel();

    /**
     * @return the panel that contains the black player stopwatch
     */
    JPanel getBlackPanel();

    /**
     * @return the panel that shows the play time
     */
    JPanel getPlayPanel();

    /**
     * Method that change the visibilty of timer panels.
     */
    void setTimerPanelVisible();

}
