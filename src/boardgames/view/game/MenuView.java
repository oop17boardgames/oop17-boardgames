package boardgames.view.game;

/**
 * Interface that that models a generic menu.
 *
 */
public interface MenuView {

    /**
     * Method that sets the main menù visible.
     */
    void showMenu();
}
