package boardgames.view.game;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import boardgames.controller.GameControllerImpl;

import boardgames.utility.BoardGame;
import boardgames.view.utility.GameFrameFactory;
import boardgames.view.utility.MainFrameFactory;

/**
 * that draw the boot menù graphics and launch the game view according to the
 * type of game chosen.
 *
 **/
public final class MenuViewImpl implements MenuView {

    private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    private static final double PERC_RESIZE_MENU = 0.3;

    private static final int DIM_FONT = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() * 0.08);

    private static final String GAME_NAME = "Board Games";
    private static final String CHECKBOX_MESSAGE = "Play with time";
    private static final String CHESS = "Chess";
    private static final String CHECKERS = "Checkers";
    private static final String EXIT = "Exit";

    private final JFrame frame;
    private final JCheckBox checkBox;

    private final GameControllerImpl controller;

    /**
     * Constructor that initialize and define all components.
     * 
     * @param c controller of entire application
     */
    public MenuViewImpl(final GameControllerImpl c) {
        this.controller = c;

        final GameFrameFactory factory = new MainFrameFactory();
        this.frame = factory.createFrame(Optional.of(GAME_NAME));
        final JPanel panel = factory.cratePanel(Optional.empty(), new GridBagLayout(), true);
        final JLabel label = factory.createJLabel(Optional.of(GAME_NAME));
        this.checkBox = factory.createCheckBox(Optional.of(CHECKBOX_MESSAGE));

        final ActionListener ac = e -> {
            final BoardGame gameType;
            final JButton jb = (JButton) e.getSource();
            gameType = jb.getText().equals("Chess") ? BoardGame.CHESS : BoardGame.CHECKERS;
            this.controller.setGameConfiguration(gameType, this.playWithTime());
            this.frame.dispose();
        };

        final JButton chess = factory.createButton(Optional.of(ac), Optional.of(CHESS));
        final JButton checkers = factory.createButton(Optional.of(ac), Optional.of(CHECKERS));
        final JButton exit = factory.createButton(Optional.of(e -> System.exit(0)), Optional.of(EXIT));

        this.frame.setSize((int) (this.screenSize.getWidth() * PERC_RESIZE_MENU),
                (int) (this.screenSize.getHeight() * PERC_RESIZE_MENU));

        try {
            label.setFont(this.getFont());
        } catch (FontFormatException | IOException e1) {
            System.out.println("File not found");
        }

        panel.setLayout(new GridBagLayout());

        final GridBagConstraints cnst = factory.createGridBagConstraints();

        panel.add(label, cnst);
        cnst.gridy++;
        panel.add(checkBox, cnst);
        cnst.gridy++;
        cnst.anchor = GridBagConstraints.WEST;
        panel.add(chess, cnst);
        cnst.gridx = 0;
        cnst.anchor = GridBagConstraints.EAST;
        panel.add(checkers, cnst);
        cnst.fill = GridBagConstraints.HORIZONTAL;
        cnst.gridx = 0;
        cnst.gridy++;
        panel.add(exit, cnst);

        exit.addActionListener(e -> System.exit(0));

        this.frame.getContentPane().add(panel);
        this.frame.setLocationRelativeTo(null);
    }

    @Override
    public void showMenu() {
        this.frame.setVisible(true);
    }

    /**
     * @return font from classpath
     * @throws FontFormatException
     * @throws IOException
     */
    private Font getFont() throws FontFormatException, IOException {
        final InputStream in = MenuViewImpl.class.getResourceAsStream("/font/theBreakdown.ttf");
        final Font font = Font.createFont(Font.TRUETYPE_FONT, in);
        return font.deriveFont(Font.PLAIN, DIM_FONT);
    }

    /**
     * @return true if is a game with timer
     */
    private boolean playWithTime() {
        return this.checkBox.isSelected();
    }

}
