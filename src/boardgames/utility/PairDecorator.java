package boardgames.utility;

/**
 * This abstract class can be used to decorate the Pair.
 * @param <X> the x coordinate of the Pair
 * @param <Y> the y coordinate of the Pair
 */
public abstract class PairDecorator<X, Y> implements Pair<X, Y> {

    private final Pair<X, Y> decorated;

    /**
     * Initializes the Pair to decorate.
     * @param pair the Pair that has to be decorated
     */
    public PairDecorator(final Pair<X, Y> pair) {
        this.decorated = pair;
    }

    /* (non-Javadoc)
     * @see boardgames.utility.Pair#getX()
     */
    @Override
    public X getX() {
        return this.decorated.getX();
    }

    /* (non-Javadoc)
     * @see boardgames.utility.Pair#getY()
     */
    @Override
    public Y getY() {
        return this.decorated.getY();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.decorated.hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        return this.decorated.equals(obj);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.decorated.toString();
    }
}
