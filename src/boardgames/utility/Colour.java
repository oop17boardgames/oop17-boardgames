package boardgames.utility;

/**
 * This enumeration defines the two type of possible colour.
 */
public enum Colour {

    /**
     * colours. 
     */
    White, Black;
}
