package boardgames.utility;

import java.util.List;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;

/**
 * This class implements the utility functions required by part types.
 */
public class PieceUtilsImpl implements PieceUtils {

    /**
     * indicates the n dimension of the board n*n.
     */
    public static final int BOARD_SIZE = 7;

    @Override
    public final boolean checkRange(final int x, final int y) {
        return (((x >= 0) && (x <= BOARD_SIZE)) && ((y >= 0) && (y <= BOARD_SIZE)));
    }

    @Override
    public final void checkRangeMoves(final List<Box> moves) {
        moves.removeIf(box -> (box.getX() < 0) || (box.getX() > BOARD_SIZE) || (box.getY() < 0) || (box.getY() > BOARD_SIZE));
    }

    @Override
    public final boolean checkOpponent(final int x, final int y, final Colour c, final BoardImpl b) {
       if (!b.getBox(x, y).get().isEmpty()) {
           return b.getBox(x, y).get().getPiece().get().getColour() != c;
       }
       return true;
    }

    @Override
    public final void checkBoxMoves(final Colour colour, final List<Box> moves, final BoardImpl b) {
        moves.removeIf(box -> (!box.isEmpty()) && (colour == box.getPiece().get().getColour()));
    }
}
