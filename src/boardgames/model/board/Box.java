package boardgames.model.board;

import java.util.Optional;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.PairDecorator;
import boardgames.utility.PairImpl;

/**
 * This class decorates a Pair via PairDecorator adding the concept of container of pieces.
 */
public class Box extends PairDecorator<Integer, Integer> {

    private Optional<PieceImpl> pieceInBox = Optional.empty();

    /**
     * Sets the coordinates and the piece of the box.
     * @param coordinate the PairImpl indicating the coordinates
     * @param currentPiece the Optional of PieceImpl indicating the Piece of the Box
     */
    public Box(final PairImpl<Integer, Integer> coordinate, final Optional<PieceImpl> currentPiece) {
            super(coordinate);
            this.pieceInBox = currentPiece;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Box other = (Box) obj;
        if (this.getX() == null) {
            if (other.getX() != null) {
                return false;
            }
        } else if (!this.getX().equals(other.getX())) {
            return false;
        }
        if (this.getY() == null) {
            if (other.getY() != null) {
                return false;
            }
        } else if (!this.getY().equals(other.getY())) {
            return false;
        }
        return true;
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.getX() == null) ? 0 : this.getX().hashCode());
        result = prime * result + ((this.getY() == null) ? 0 : this.getY().hashCode());
        return result;
    }

    /**
     * @return true if the Box is empty, otherwise false.
     */
    public boolean isEmpty() {
        return !this.pieceInBox.isPresent();
    }

    /**
     * Puts currentPiece in this Box.
     * @param currentPiece the Piece that have to be putted in this Box
     */
    public void setPiece(final Optional<PieceImpl> currentPiece) {
        this.pieceInBox = currentPiece;
    }

    /**
     * @return a Pair representing the coordinate of this Box.
     */
    public PairImpl<Integer, Integer> getPair() {
        return new PairImpl<>(this.getX(), this.getY());
    }

    /**
     * @return an Optional containing the Piece that is in this Box, otherwise an empty optional.
     */
    public Optional<PieceImpl> getPiece() {
        return this.pieceInBox;
    }

    @Override
    public final String toString() {
        if (this.isEmpty()) {
            return super.toString();
        } else {
            return super.toString() + " " + this.getPiece().get().getName();
        }
    }

}
