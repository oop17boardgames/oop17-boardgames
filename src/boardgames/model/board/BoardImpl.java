package boardgames.model.board;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;
import boardgames.utility.PieceUtils;
import boardgames.utility.PieceUtilsImpl;

/**
 * This class implements the functions useful for modeling the common Board.
 */
public class BoardImpl implements Board {

    private List<List<Box>> allPiecesOnBoard;
    private List<PieceImpl> whitePieces;
    private List<PieceImpl> blackPieces;
    private Optional<BoardType> type = Optional.empty();
    private static final Logger LOGGER = LogManager.getLogger(BoardImpl.class.getName());
    private final PieceUtils pu = new PieceUtilsImpl();

    @Override
    public final void setBoardType(final BoardType type) {
        this.type = Optional.of(type);
    }

    @Override
    public final Optional<BoardType> getBoardType() {
        return this.type;
    }

    @Override
    public final void reset() {
        try {
            this.allPiecesOnBoard = this.type.get().reset();
            this.whitePieces = this.type.get().getWhiteStartPieces();
            this.blackPieces = this.type.get().getBlackStartPieces();
        } catch (NoSuchElementException e) { 
            LOGGER.debug("BoardType not initialized");
        }
    }

    @Override
    public final Optional<Box> getBox(final int x, final int y) {
        if (!this.pu.checkRange(x, y)) {
            LOGGER.debug("Coordinates out of range when getting a box");
            throw new IllegalArgumentException();
        } else {
            return Optional.of(this.allPiecesOnBoard.get(x).get(y));
        }
    }

    @Override
    public final List<List<Box>> getAllBoxes() {
        return this.allPiecesOnBoard;
    }

    @Override
    public final List<PieceImpl> getWhitePieces() {
        return this.whitePieces;
   }

    @Override
    public final void addWhitePiece(final PieceImpl pw) {
        if (!pw.getColour().equals(Colour.White)) {
            LOGGER.debug("White player can't has a " + pw.getColour().toString() + " piece");
            throw new IllegalArgumentException();
        } else {
            this.whitePieces.add(pw);
        }
    }

    @Override
    public final void removeWhitePiece(final PieceImpl pb) {
        this.whitePieces.remove(pb);
    }

    @Override
    public final List<PieceImpl> getBlackPieces() {
        return this.blackPieces;
    }

    @Override
    public final void addBlackPiece(final PieceImpl pb) {
        if (!pb.getColour().equals(Colour.Black)) {
            LOGGER.debug("Black player can't has a " + pb.getColour().toString() + " piece");
            throw new IllegalArgumentException();
        } else {
            this.blackPieces.add(pb);
        }
    }

    @Override
    public final void removeBlackPiece(final PieceImpl pb) {
        this.blackPieces.remove(pb);
    }
}
