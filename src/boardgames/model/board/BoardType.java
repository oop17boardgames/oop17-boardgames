package boardgames.model.board;

import java.util.List;

import boardgames.model.piece.PieceImpl;

/**
 * This interface defines the functions useful to define new type of Board.
 */
public interface BoardType {

    /**
     * @return a List of List of Box representing the Board initialized
     */
    List<List<Box>> reset();

    /**
     * @return a List containing white's Pieces
     */
    List<PieceImpl> getWhiteStartPieces();

    /**
     * @return a List containing black's Pieces
     */
    List<PieceImpl> getBlackStartPieces();
}
