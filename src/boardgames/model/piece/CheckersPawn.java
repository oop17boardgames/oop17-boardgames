package boardgames.model.piece;

import java.util.LinkedList;
import java.util.List;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.utility.CheckersUtilsImpl;
import boardgames.utility.Colour;

/**
 *
 */
public class CheckersPawn implements PieceType {

    private final CheckersUtilsImpl cu;
    private static final  String NAME = "Pawn";
    private final List<Box> moves;

    /**
     * 
     */
    public CheckersPawn() {
        this.cu = new CheckersUtilsImpl();
        this.moves = new LinkedList<>();
    }

    @Override
    public final List<Box> possibleMoves(final BoardImpl b, final Box current, final Colour colour) {
        this.moves.clear();
        this.cu.getPaths().clear();

        if (colour == Colour.White) {
            this.cu.calcWhiteEatMoves(b, current, colour);
        } else {
            this.cu.calcBlackEatMoves(b, current, colour);
        }

        this.cu.getPaths().forEach((destination, path) -> this.moves.add(destination));
        if (this.moves.isEmpty()) {
            if (colour == Colour.White) {
                this.cu.whiteMoves(b, current, moves);
            } else {
                this.cu.blackMoves(b, current, moves);
            }
        }
        return this.moves;
    }

    /**
      * @param newPosition the Box where the piece is moved
     *  @return a List containing the piece eated doing the move indicated
     */
    public final List<PieceImpl> getPossibleEated(final Box newPosition) {
        return this.cu.getPossibleEated(newPosition);
    }

    @Override
    public final String getName() {
        return CheckersPawn.NAME;
    }
}
