package boardgames.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import boardgames.model.CheckTimedWinner;
import boardgames.model.CheckTimedWinnerImpl;
import boardgames.model.CheckersGame;
import boardgames.model.ChessGame;

import boardgames.model.GameImpl;
import boardgames.model.board.Board;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.BoardGame;
import boardgames.utility.Colour;
import boardgames.view.board.BoardView;
import boardgames.view.board.CheckersBoardView;
import boardgames.view.board.ChessBoardView;
import boardgames.view.game.CheckersView;
import boardgames.view.game.ChessView;
import boardgames.view.game.GameView;

/**
 * This class implement the controller responsible of the game that will be
 * launched.
 * it manages the game phases such as the initialization, the movement and the elimination of 
 * the pieces based on what is communicated by view and model.
 */
public final class GameControllerImpl implements GameController {

    private static final GameControllerImpl SINGLETON = new GameControllerImpl();
    private static final Integer BOARD_DIMENSION = 8;

    private static final Logger LOGGER = LogManager.getLogger(GameControllerImpl.class.getName());

    private boolean timed;
    private boolean isFirstMove;
    private GameImpl model;
    private GameView gView;
    private Optional<PieceImpl> selectedPiece;
    private Optional<Box> pieceMovedIn;
    private Board modelBoard;
    private BoardView boardView;
    private Colour turn;
    private GameStopwatch timer;
    private CheckTimedWinner ctw;

    private GameControllerImpl() {
        this.turn = Colour.White;
        this.selectedPiece = Optional.empty();
        this.isFirstMove = true;

    }

    /** Get the unique GameControllerImpl instance of the application.
     * @return the unique instance of this class
     * 
     */
    public static GameControllerImpl getController() {
        return SINGLETON;
    }

    @Override
    public void setGameConfiguration(final BoardGame game, final boolean isTimed) {
        final BoardGame gameType = game;
        this.timed = isTimed;
        if (gameType.equals(BoardGame.CHESS)) {
            LOGGER.trace("Chess game has been chosen");
            this.model = new ChessGame();
            this.boardView = new ChessBoardView(this, BOARD_DIMENSION);
            this.gView = new ChessView(this, this.boardView, isTimed);
        } else {
            LOGGER.trace("Checkers game has been chosen");
            this.model = new CheckersGame();
            this.boardView = new CheckersBoardView(this, BOARD_DIMENSION);
            this.gView = new CheckersView(this, this.boardView, isTimed);
        }

        this.modelBoard = model.getBoard();

        this.gView.drawGraphics();
        this.model.attach(this.gView);

        this.timer = new GameStopwatch();

        if (isTimed) {
            LOGGER.trace("Timed match");
            timer.wStart();
            this.ctw = new CheckTimedWinnerImpl(this.model);
        } else {
            timer.playTimeStart();
        }

    }

    @Override
    public Board getGameBoard() {
        return this.modelBoard;
    }

    @Override
    public List<PieceImpl> getGamePieces() {
        final List<PieceImpl> allPieces = new ArrayList<>();
        allPieces.addAll(this.modelBoard.getBlackPieces());
        allPieces.addAll(this.modelBoard.getWhitePieces());
        return allPieces;

    }

    @Override
    public List<Box> getPossibleMoves(final Optional<PieceImpl> p) {
        if (this.isRightTurn()) {
                return p.get().possibleMoves(model.getBoard());
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public void setPieceToMove(final Optional<PieceImpl> p) {
        this.selectedPiece = p;

    }

    @Override
    public List<PieceImpl> getEatedPiece() {
        return model.getLastPieceEated();
    }

    @Override
    public Optional<PieceImpl> getPieceToMove() {
        return this.selectedPiece;
    }

    @Override
    public boolean isRightTurn() {
        return selectedPiece.get().getColour() == turn;
    }

    @Override
    public void movePieceTo(final Optional<Box> b) {
        if (this.isRightTurn()) {
            LOGGER.debug("selectedPiece impostato in precedenza è nel box:" + selectedPiece.get().getBox());
            LOGGER.debug("Posizione passata da view in cui spostare il pezzo: " + b);

            if (model.move(turn, selectedPiece.get(), b)) {
                this.changeTurn();
                this.gView.drawMove(selectedPiece.get().getBox(), b);
                this.model.updateMove(selectedPiece.get(), b);
                this.pieceMovedIn = b;
                setPieceToMove(Optional.empty());

            }
        }
    }

    @Override
    public Colour getTurn() {
        return this.turn;
    }

    @Override
    public void changeTurn() {
        if (timed) {
        this.arrangeTimers();
        }
        this.turn = turn.equals(Colour.Black) ? Colour.White : Colour.Black;
        LOGGER.trace("Is now " + turn + " turn");
    }

    @Override
    public void arrangeTimers() {
            if (isFirstMove) {
                timer.bStart();
                LOGGER.debug("Black timer has started");
                GameStopwatch.getWstopwatch().suspend();
                isFirstMove = false;
            } else {
                if (turn.equals(Colour.White)) {
                    LOGGER.debug("White stopwatch has suspended");
                    GameStopwatch.getWstopwatch().suspend();
                    LOGGER.debug("Black stopwatch has resumed");
                    GameStopwatch.getBstopwatch().resume();
                } else if (turn.equals(Colour.Black)) {
                    LOGGER.debug("White stopwatch has resumed");
                    GameStopwatch.getWstopwatch().resume();
                    LOGGER.debug("Black stopwatch has suspended");
                    GameStopwatch.getBstopwatch().suspend();
                }
            }

    }

    @Override
    public void doPiecePromotion(final String upgraded) {
        model.promotion(pieceMovedIn.get().getPiece().get().getColour(), upgraded);
    }

    @Override
    public Optional<Box> pieceMovedTo() {
        return this.pieceMovedIn;
    }

    @Override
    public Colour getOppositePlayerColour() {
        return turn.equals(Colour.Black) ? Colour.White : Colour.Black;
    }

    @Override
    public Optional<Colour> getTimedWinner() {
        return this.ctw.checkPieceNumber();
    }

    @Override
    public boolean isTimed() {
        return timed;
    }

}
