
package boardgames.controller;

import boardgames.utility.BoardGame;
import boardgames.utility.Colour;

import java.util.List;
import java.util.Optional;

import boardgames.model.board.Board;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;


/**
 * This interface model the controller responsible of the game that will be
 * launched.
 *
 */
public interface GameController {

    /**
     * Method that initializes which game will be played, which board configuration
     * will have to use, if it is timed and which graphical aspect it will have to
     * have.
     * 
     * @param game
     *            the game type, (Chess or Checkers)
     * @param isTimed
     *            (true if is a game timed)
     */
    void setGameConfiguration(BoardGame game, boolean isTimed);

    /**
     * Method that provides the model's implementations of game pieces.
     * 
     * @return a list of game pieces
     */
    List<PieceImpl> getGamePieces();

    /**
     * puts in communication model and view, asking the first one if the piece can
     * move, and informing the second one to report these changes at user interface
     * level.
     * 
     * @param b
     *            box where the piece will be moved
     */
    void movePieceTo(Optional<Box> b);

    /**
     * Method that provides the model's board.
     * 
     * @return the game board
     */
    Board getGameBoard();

    /**
     * Pass to the model the piece that must be "promoted".
     * 
     * @param upgraded 
     */
    void doPiecePromotion(String upgraded);

    /**
     * Sets the piece that will be moved this turn.
     * 
     * @param p
     *            piece to move
     */
    void setPieceToMove(Optional<PieceImpl> p);

    /**
     * @return the previously selected piece waiting to be moved
     */
    Optional<PieceImpl> getPieceToMove();

    /**
     * takes from the model the last piece eaten during the game.
     * 
     * @return the pieces eaten and then removed from the game
     */
    List<PieceImpl> getEatedPiece();

    /**
     * @return true if piece colour correspond to the colour of turn
     */
    boolean isRightTurn();

    /**
     * Method that get from model the possible moves for one piece.
     * 
     * @param p
     *            piece to watch the moves
     * @return list of possible moves of a piece, which the player needs to decide
     *         the move. If he has none return an empty list;
     */
    List<Box> getPossibleMoves(Optional<PieceImpl> p);

    /**
     * @return the color of the player who will make the move
     */
    Colour getTurn();

    /**
     * @return White, if turn is equals to black, black otherwise.
     */
    Colour getOppositePlayerColour();

    /**
     * @return the Colour of the player with more eated pieces, provided by the
     *         model
     */
    Optional<Colour> getTimedWinner();

    /**
     * set the game turn, changing its color.
     */
    void changeTurn();

    /**
     * suspend and restart the game timers at each change of turn.
     */
    void arrangeTimers();

    /**
     * @return the Box where the last piece has been moved
     */
    Optional<Box> pieceMovedTo();

    /**
     * @return true if it's a timed game
     */
    boolean isTimed();
}
