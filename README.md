#OOP17-boardgames
L'obiettivo dell'applicazione � quello di realizzare una versione virtuale dei giochi "Scacchi" e "Dama".


##Utilizzo

Sistemi Windows -> eseguire il jar
Sistemi Linux -> utilizzare da terminale il comando: java -jar Boardgames.jar 


##Librerie esterne utilizzate

* Log4j 2
* Apache Commons

##Contatti
* Filippo Paganelli filippo.paganelli3@studio.unibo.it
* Linda Vitali linda.vitali2@studio.unibo.it

